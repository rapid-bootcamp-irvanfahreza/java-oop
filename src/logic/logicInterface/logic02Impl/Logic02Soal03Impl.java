package logic.logicInterface.logic02Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic02Soal03Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic02Soal03Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        for (int i = 0; i < this.logic.n; i++) {
            int angka = 0;
            int awal = 0;
            int akhir = 16;
            for (int j = 0; j < this.logic.n; j++) {
                if(i==j || i+j == this.logic.n-1){
                    this.logic.array[i][j] = String.valueOf(angka);
                }else if(i>j && j==0){
                    this.logic.array[i][j] = String.valueOf(awal);
                } else if (i<j && j==8 ){
                    this.logic.array[i][j] = String.valueOf(akhir);
                } else if (i<j && i==0 || i>j && i==this.logic.n-1){
                    this.logic.array[i][j] = String.valueOf(angka);
                }
                angka+=2;
            }
        }
    }
    public void isiArray2(){
        for (int i = 0; i < this.logic.n; i++) {
            int x = 0;
            int y = 0;
            int z = 16;
            for (int j = 0; j < this.logic.n; j++) {
                if(i==j || i+j == this.logic.n-1){
                    this.logic.array[i][j] = String.valueOf(x);
                }else if(i>j && j==0){
                    this.logic.array[i][j] = String.valueOf(y);
                } else if (i<j && j==8 ){
                    this.logic.array[i][j] = String.valueOf(z);
                } else if (i<j && i==0 || i>j && i==this.logic.n-1){
                    this.logic.array[i][j] = String.valueOf(x);
                }
                x+=2;
            }
        }
    }
    public void isiArray3(){
        for (int i = 0; i < this.logic.n; i++) {
            int a = 0;
            int b = 0;
            int c = 16;
            for (int j = 0; j < this.logic.n; j++) {
                if(i==j || i+j == this.logic.n-1){
                    this.logic.array[i][j] = String.valueOf(a);
                }else if(i>j && j==0){
                    this.logic.array[i][j] = String.valueOf(b);
                } else if (i<j && j==8 ){
                    this.logic.array[i][j] = String.valueOf(c);
                } else if (i<j && i==0 || i>j && i==this.logic.n-1){
                    this.logic.array[i][j] = String.valueOf(a);
                }
                a+=2;
            }
        }
    }
    public void isiArray4(){
        for (int i = 0; i < this.logic.n; i++) {
            int p = 0;
            int q = 0;
            int r = 16;
            for (int j = 0; j < this.logic.n; j++) {
                if(i==j || i+j == this.logic.n-1){
                    this.logic.array[i][j] = String.valueOf(p);
                }else if(i>j && j==0){
                    this.logic.array[i][j] = String.valueOf(q);
                } else if (i<j && j==8 ){
                    this.logic.array[i][j] = String.valueOf(r);
                } else if (i<j && i==0 || i>j && i==this.logic.n-1){
                    this.logic.array[i][j] = String.valueOf(p);
                }
                p+=2;
            }
        }
    }
    public void isiArray5(){
        for (int i = 0; i < this.logic.n; i++) {
            int number = 0;
            int starts = 0;
            int ends = 16;
            for (int j = 0; j < this.logic.n; j++) {
                if(i==j || i+j == this.logic.n-1){
                    this.logic.array[i][j] = String.valueOf(number);
                }else if(i>j && j==0){
                    this.logic.array[i][j] = String.valueOf(starts);
                } else if (i<j && j==8 ){
                    this.logic.array[i][j] = String.valueOf(ends);
                } else if (i<j && i==0 || i>j && i==this.logic.n-1){
                    this.logic.array[i][j] = String.valueOf(number);
                }
                number+=2;
            }
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.print();
    }
    public void cetakArray2() {
        this.isiArray2();
        this.logic.print();
    }
    public void cetakArray3() {
        this.isiArray3();
        this.logic.print();
    }
    public void cetakArray4() {
        this.isiArray4();
        this.logic.print();
    }
    public void cetakArray5() {
        this.isiArray5();
        this.logic.print();
    }
}
