package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal04Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal04Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int[][] deret = new int[logic.n][logic.n];
        for (int i = 0; i < deret.length ; i++) {
            if(i==0 || i==1){
                deret[0][i] = 1;
            } else {
                deret[0][i] = deret [0][i-1] + deret[0][i-2];
            }
            this.logic.array[0][i] = String.valueOf(deret[0][i]);
        }
    }
    public void isiArray2(){
        int[][] baris = new int[logic.n][logic.n];
        for (int i = 0; i < baris.length ; i++) {
            if(i==0 || i==1){
                baris[0][i] = 1;
            } else {
                baris[0][i] = baris [0][i-1] + baris[0][i-2];
            }
            this.logic.array[0][i] = String.valueOf(baris[0][i]);
        }
    }
    public void isiArray3(){
        int[][] index = new int[logic.n][logic.n];
        for (int i = 0; i < index.length ; i++) {
            if(i==0 || i==1){
                index[0][i] = 1;
            } else {
                index[0][i] = index [0][i-1] + index[0][i-2];
            }
            this.logic.array[0][i] = String.valueOf(index[0][i]);
        }
    }
    public void isiArray4(){
        int[][] x = new int[logic.n][logic.n];
        for (int i = 0; i < x.length ; i++) {
            if(i==0 || i==1){
                x[0][i] = 1;
            } else {
                x[0][i] = x [0][i-1] + x[0][i-2];
            }
            this.logic.array[0][i] = String.valueOf(x[0][i]);
        }
    }
    public void isiArray5(){
        int[][] a = new int[logic.n][logic.n];
        for (int i = 0; i < a.length ; i++) {
            if(i==0 || i==1){
                a[0][i] = 1;
            } else {
                a[0][i] = a [0][i-1] + a[0][i-2];
            }
            this.logic.array[0][i] = String.valueOf(a[0][i]);
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
    public void cetakArray2() {
        this.isiArray2();
        this.logic.printSingle();
    }
    public void cetakArray3() {
        this.isiArray3();
        this.logic.printSingle();
    }
    public void cetakArray4() {
        this.isiArray4();
        this.logic.printSingle();
    }
    public void cetakArray5() {
        this.isiArray5();
        this.logic.printSingle();
    }
}
