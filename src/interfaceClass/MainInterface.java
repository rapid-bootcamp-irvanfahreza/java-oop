package interfaceClass;

public class MainInterface {
    public static void main(String[] args) {
        HasBrand brand = new HasBrandImpl("Pepsodent", 25000);
        System.out.println("Brand name: "+brand.getBrand()+", Price: Rp."+ brand.getPrice());
    }
}
