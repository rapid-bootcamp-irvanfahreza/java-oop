package finalMethod;

public abstract class Motor {
    String nama;
    Double harga;

    abstract void run();
}
