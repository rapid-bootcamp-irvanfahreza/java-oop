package encapsulation;

public class MainCategory {
    public static void main(String[] args) {

        // cara 1
        Category category = new Category();
        category.setId(1);
        category.setName("makanan");
        category.setDesc("ayam");
        System.out.println(category);

        // cara 2 (lebih simple)
        System.out.println(new Category(2, "Minuman","Coca-cola"));
        System.out.println(new Category(3, "Alat Olahraga","Barbel"));

    }
}
